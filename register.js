"use strict"
let reqForm = document.getElementsByClassName("requiredFields")[0]
let reqFields = reqForm.getElementsByTagName("INPUT");





function validateForm() { // validating the forms
  let isValid = true;
  for (let i = 0; i < reqFields.length; i++) {
    let parent = reqFields[i].parentElement
    if (reqFields[i].nextSibling) {
      reqFields[i].parentNode.removeChild(reqFields[i].nextSibling)

    }

    if (!reqFields[i].value) {
      isValid = false;
      let errorNode = document.createElement("p"); // Create a <p> node 
      let errorNodeText = document.createTextNode("this field is required"); // Create a text for the error
      errorNode.classList.add("reqField")
      errorNode.appendChild(errorNodeText);
      parent.appendChild(errorNode);

    }

  }
  return isValid
}