"use strict"
let indexRow = 4; // amount of Rows per page to be displayed
let i = 1; // counter of the rows displayed
let rowCount = document.getElementsByClassName("productList")[0].getElementsByTagName("section").length; // counting the number of rows with items (3 items on each row by design)
let totalPages = Math.ceil(rowCount / 4); // calculating total number of pages s per line)
let currentPage = Math.ceil(i / 4); // show number of displayed page on load

document.getElementsByClassName("regButton")[0].onclick = load_register;

function load_register() {
    document.getElementsByClassName("registerFormDiv")[0].classList.toggle("activeForm");
    document.getElementsByTagName("main")[0].classList.toggle("fitReg");
}

document.getElementsByClassName("nextPageBtn")[0].onclick = nextPage;
document.getElementsByClassName("previousPageBtn")[0].disabled = "disabled";



function checkDisableNext() {

    if (i + indexRow > rowCount) {
        document.getElementsByClassName("nextPageBtn")[0].disabled = "disabled";
    } else {
        document.getElementsByClassName("nextPageBtn")[0].disabled = "";
        document.getElementsByClassName("nextPageBtn")[0].onclick = nextPage;
    }
};

function checkDisablePrev() {
    console.log(i, indexRow, rowCount);
    if (i + indexRow <= rowCount) {
        document.getElementsByClassName("previousPageBtn")[0].disabled = "disabled";
    } else {
        document.getElementsByClassName("previousPageBtn")[0].disabled = "";
        document.getElementsByClassName("previousPageBtn")[0].onclick = previousPage;
    }
};

document.getElementsByClassName("currentPage")[0].innerHTML = `Page ${currentPage} out of ${totalPages}`; // displaying total number of Pages

for (let e = 1; e <= rowCount; e++) { // everything invisible on load
    document.getElementsByClassName(`itemRow${e}`)[0].classList.remove("active");
}

for (let r = 1; r <= indexRow; r++) { // displaying first page items
    document.getElementsByClassName(`itemRow${r}`)[0].classList.add("active");
}


function displayPageNumber() {
    currentPage = Math.ceil(i / 4);
    document.getElementsByClassName("currentPage")[0].innerHTML = `Page ${currentPage} out of ${totalPages}`;
}


function previousPage() {
    i -= indexRow
    for (let e = 1; e <= rowCount; e++) { // everything invisible on load
        document.getElementsByClassName(`itemRow${e}`)[0].classList.remove("active")
    }


    for (let r = i; r < i + indexRow; r++) {
        document.getElementsByClassName(`itemRow${r}`)[0].classList.add("active");

    }
    displayPageNumber();
    checkDisableNext();
    checkDisablePrev();
}



function nextPage() {
    i += indexRow
    for (let e = 1; e <= rowCount; e++) { // everything invisible on load
        document.getElementsByClassName(`itemRow${e}`)[0].classList.remove("active")
    }

    if (i + indexRow > rowCount) {
        for (let r = i; r <= rowCount; r++) {
            document.getElementsByClassName(`itemRow${r}`)[0].classList.add("active")
        }
    } else {
        for (let r = i; r <= i + indexRow; r++) {
            document.getElementsByClassName(`itemRow${r}`)[0].classList.add("active")
        }
    }
    displayPageNumber()
    checkDisableNext()
    checkDisablePrev()

}